#!/bin/bash
set -e
read -p "prerequisites: postgres, postgis, virtualenv (python3), fabric, redis, npm. All good? " -n 1 -r
echo ''
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # let's skip this for now...
    # git remote add -f community_base 'git@bitbucket.org:spookyvision/community_base.git'
    # git remote add -f polly_bob 'git@bitbucket.org:spookyvision/polly_bob.git'
    # git subtree add --prefix=community_base community_base master --squash
    # git subtree add --prefix=polly_bob polly_bob master --squash

    mkvirtualenv polly_bob
    pip install -r community_base/requirements.txt
    npm install -g gulp
    npm install
    mkdir static
    mkdir -p static-out/js
    mkdir -p static-out/css
    fab prepare_assets
    createdb polly_bob
    psql -U postgres -d polly_bob -c 'create extension postgis; create extension postgis_topology;'
    ./manage.py migrate
    DJANGO_SETTINGS_MODULE=polly_bob_site.settings ./polly_bob/fill_db.py
fi
