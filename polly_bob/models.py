# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import six
from django.conf import settings
from django.contrib import admin
from django.contrib.gis.db import models
from django.db.models import Max
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from sorl.thumbnail import get_thumbnail

from community_base.models import CBUser, Category, BaseImage

LISTING_THUMBNAIL_GEOMETRY='200x200'

@six.python_2_unicode_compatible
class Achievement(models.Model):
    description = models.CharField(blank=False, null=False, max_length=40)
    points = models.PositiveIntegerField()
    members = models.ManyToManyField(CBUser, through='AchievementAward')

    def __str__(self):
        return 'Achievement %s points: %s' % (self.points, self.description)


@six.python_2_unicode_compatible
class AchievementAward(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    achievement = models.ForeignKey(Achievement)
    user = models.ForeignKey(CBUser)

    def __str__(self):
        return '%s-%s' % (self.user, self.achievement)

@six.python_2_unicode_compatible
class Group(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    name = models.CharField(max_length=120)
    image = models.ImageField(upload_to='images')
    description = models.TextField()
    admins = models.ManyToManyField(CBUser, related_name='administered_groups', blank=True)
    members = models.ManyToManyField(CBUser, related_name='groups', blank=True)
    position = models.PointField(null=True)
    def to_list_display(self):
        return {
            'image': self.thumbnail_image,
            'title': self.name,
            'text': self.description,
            'category':None,
            'creator':None,
            'url': reverse('pb_group', kwargs={'group_id':self.pk}),
            'position': self.position,
            'distance': self.distance.m,
        }
    @property
    def thumbnail_image(self):
        return get_thumbnail(self.image, LISTING_THUMBNAIL_GEOMETRY).url
    @property
    def large_image(self):
        return get_thumbnail(self.image, '1280x400', crop='top').url


class GroupThread(models.Model):
    group = models.ForeignKey(Group, related_name='threads')
    created_at = models.DateTimeField(auto_now_add=True)

@six.python_2_unicode_compatible
class Listing(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=120)
    image = models.ImageField(upload_to='images')
    text = models.TextField()
    creator = models.ForeignKey(CBUser, related_name='listings')
    watchers = models.ManyToManyField(CBUser, related_name='watched_listings')
    category = models.ForeignKey(Category, related_name='listings', null=True)
    position = models.PointField(null=True)

    def to_list_display(self):
        return {'image': self.thumbnail_image,
                'title': self.title,
                'text': self.text,
                'category':self.category,
                'creator': self.creator,
                'url': reverse('pb_listing', kwargs={'listing_id':self.pk}),
                'position': self.position,
                'distance': self.distance.m}

    def __str__(self):
        return 'Listing #%s in %s (%s): %s' % (self.pk, self.creator.zip_code, self.category, self.title[:20])

    @property
    def heading_image(self):
        return get_thumbnail(self.image, '500x200', crop='top').url
    @property
    def thumbnail_image(self):
        return get_thumbnail(self.image, LISTING_THUMBNAIL_GEOMETRY, crop='top').url
    @property
    def large_image(self):
        return get_thumbnail(self.image, '1280x400', crop='top').url
    @property
    def cs(self):
        return list(self.comments.all())

@six.python_2_unicode_compatible
class Comment(models.Model):
    class Meta:
        ordering = ['created_at']
    created_at = models.DateTimeField(auto_now_add=True)
    text = models.TextField(blank=False, null=False)
    author = models.ForeignKey(CBUser, related_name='comments', null=False)


class ListingComment(Comment):
    listing = models.ForeignKey(Listing, related_name='comments')
    def save(self, *args, **kwargs):
        super(Comment, self).save()
        #TODO this is very wrong - need to notify all thread commenters minus current author
        if self.author != self.listing.creator:
            self.author.profile.unread.add(self.listing)
    def __str__(self):
        return 'Comment for listing %s by %s: %s' % (self.listing.pk, self.author, self.text[:20])

class ThreadComment(Comment):
    thread = models.ForeignKey(GroupThread, related_name='comments')

@six.python_2_unicode_compatible
class Profile(models.Model):
    IMMEDIATELY = 1
    DAILY = 2
    WEEKLY = 3

    NOTIFY_CHOICES = (
        (IMMEDIATELY, 'Immediately'),
        (DAILY, 'Daily'),
        (WEEKLY, 'Weekly'),
    )
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name='profile')

    notify_on_message = models.IntegerField(
        _('Notify on message'), choices=NOTIFY_CHOICES, null=True,
        default=IMMEDIATELY)
    notify_on_request_or_review = models.IntegerField(
        _('Notify on request or review'), choices=NOTIFY_CHOICES, null=True,
        default=IMMEDIATELY)

    categories = models.ManyToManyField(Category, blank=True)
    unread = models.ManyToManyField(Listing, blank=True)
    photo = models.ImageField(upload_to='images', null=True)
    about = models.TextField(null=True, blank=True)
    @property
    def timestamp(self):
        return self.user.created_at
    @property
    def thumbnail_image(self):
        if not self.photo:
            return None
        return get_thumbnail(self.photo, LISTING_THUMBNAIL_GEOMETRY, crop='top').url
    @property
    def thumbnail_small(self):
        if not self.photo:
            return None
        return get_thumbnail(self.photo, '100x100', crop='top').url
    @property
    def thumbnail_tiny(self):
        if not self.photo:
            return None
        return get_thumbnail(self.photo, '48x48', crop='top').url
    def to_list_display(self):
        return {'image': self.thumbnail_image,
                'title': self.user.display_name,
                'text': self.about,
                'category':None,
                'distance': self.distance.m,
                }

@receiver(post_save, sender=settings.AUTH_USER_MODEL,
          dispatch_uid="create_profile")
def create_profile(sender, instance, **kwargs):
    if kwargs['created']:
        Profile.objects.create(user=instance)


for admin_cls in [Listing, Comment, Achievement, AchievementAward]:
    admin.site.register(admin_cls)
