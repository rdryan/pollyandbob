#!/usr/bin/env python
# -*- coding: utf-8 -*-
# DJANGO_SETTINGS_MODULE=polly_bob_site.settings ./polly_bob/fill_db.py
from __future__ import unicode_literals

import sys
import os

sys.path.append(os.getcwd())

import django
django.setup()

from polly_bob_site import settings

from django.core.files import File
from polly_bob.models import Listing, Profile, ListingComment, GroupThread, Group, ThreadComment
from community_base.models import CBUser, Category, BaseImage
from random import choice, randint, random, shuffle
from math import sin, cos, pi
import names
from django.utils import lorem_ipsum
from django.contrib.gis.geos import Point

zips = ['10785', '10961', '10963', '10243']

for c in Group, CBUser, Listing:
    c.objects.all().delete()

for pd in Profile.objects.all():
    pd.user.delete()
    pd.delete()
Category.objects.all().delete()

def more_users():
    nlist = list(set([names.get_first_name() for x in range(10)]))
    result = []
    for n in nlist:
        result.append({
            'display_name':n,
            'email':'%s@example.com' % n.lower(),
            'zip_code':choice(zips)
        })
    return result

user_data = [
            {
                'email':'anatol@versteht.es',
                'display_name':'Anatol',
                'zip_code':'10243',
            },{
                'email':'polly@polly.bob',
                'display_name':'Polly',
                'zip_code':'10243',
                'profile': {'photo': 'volker1/ines.jpg'}
            },
        ]  + more_users()

user_data = [
    {
        'email':'polly@polly.bob',
        'display_name':'Polly',
        'zip_code':'10243',
        'profile': {
            'photo': 'volker1/out/ines.jpg',
            'about': """<div><strong>Über mich:</strong> Hi Leute, ich bin gespannt darauf, meinen Bekanntenkreis in der Nachbarschaft auszubauen.</div>
            <div><strong>Was ich mag:</strong> Musik, Gesang, Jazz, Band Gründung, Elterngruppe</div>
            <div><strong>Meine Sprachen:</strong>Deutsch, Englisch</div>
            <div><strong>Meine Gruppen:</strong>Elterngruppe, Gruppe Frankfurter Tor</div>
            """
        }
    },
    {
        'email':'Bob@polly.bob',
        'display_name':'Bob',
        'zip_code':'10243',
        'profile': {
            'photo': 'volker1/out/image7.jpg',
            'about': """<div><strong>Über mich:</strong>Hi Leute, ich bin jetzt auch hier, neu in der Stadt - eigentlich aus Pittsburgh, aber suche jetzt neue Freunde zum klettern und Deutsch lernen!</div>
            <div><strong>Was ich mag:</strong>Klettern, TableTennis, Grillen im Park, Deutsch lernen</div>
            <div><strong>Meine Sprachen:</strong>English, Deutsch (ein bisschen)</div>
            <div><strong>Meine Gruppen:</strong>Klettergruppe, Expats in the hood</div>
            """
        }
    },
    ]

img_dir = os.path.join(settings.BASE_DIR, 'polly_bob', 'fixture_data')
imgs = [i for i in os.listdir(img_dir) if i.lower().endswith('jpg')]

for idx, cv in enumerate(['Veranstaltungen', 'Nachbarn', 'Hilfe gesucht', 'Hilfsangebote', 'Dienstleistung gesucht', 'Dienstleistung angeboten', 'Sachen Gesuche', 'Sachen Angebote', 'Leute gesucht', 'Gruppen', 'Neuigkeiten']):
    Category.objects.create(value=cv, order=idx)
categories = Category.objects.all()

listings = [
    {
        'title':'Refugee Dinner',
        'image':'volker1/out/IMG_1340.JPG',
        'category':'Veranstaltungen',
        'text':"""<p>Berliner laden flüchtende Menschen zum Abendessen zu sich nach Hause ein.
        <p>Viele Menschen kommen in diesen Wochen und Monaten zu uns aus der Not von Krieg und Terror. Wir wollen sie auch bei uns zu Hause willkommen heißen und Kontakte von Mensch zu Mensch ermöglichen.
        <p>Wir laden alle ein, die eine Brücke schlagen wollen, ein paar Flüchtende, oder eine Flüchtlingsfamilie zu sich nach Hause auf ein gemeinsames Abendessen einzuladen.
        <p>Zusammen mit Friedrichshain hilft koordinieren und begleiten wir die Zusammenkunft.
        <p>Hast du Lust? Trage dich einfach in diese Liste ein. Anmeldeschluss ist der 8.12.2015, wir sagen dir am 10.12.2015 genau bescheid."""
    },
    {
        'title':'Singende Wohnzimmer',
        'image':'volker1/out/image2.jpg',
        'category':'Veranstaltungen',
        'text':'Nachbarn singen bei Nachbarn für Nachbarn. Melde dich jetzt an!'
    },
    {
        'title':'Singing Balconies',
        'image':'volker1/out/image3.jpg',
        'category':'Veranstaltungen',
        'text':'Die Nacht der Singenden Balkone von Friedrichshain. Es geht darum die Augen zu öffnen, dass wir jeden Tag aneinander vorbeilaufen und nicht daran denken, dass die Lösungen hinter den Fassaden versteckt sind.'
    },
    {
        'title':'Running Dinner',
        'image':'volker1/out/image41.png',
        'category':'Veranstaltungen',
        'text':'Ein Dinner in der Nachbarschaft. Neue Leute kennenlernen. Wer ist dabei?'
    },
    {
        'title':'Kultursonntag',
        'image':'volker1/out/image4.jpg',
        'category':'Veranstaltungen',
        'text':'Wir treffen uns jedes Mal in einem Museum oder Ausstellung oder auch zu einem Konzert und gehen danach was trinken. Diesmal ein Konzert der Band BomboCombo in der alten Kulturbrauerei.'
    },
    {
        'title':'Katze sitten',
        'image':'volker1/out/image5.jpeg',
        'category':'Hilfe gesucht',
        'text':'Wer hat Lust ab und zu meine liebe Katze Peggy zu sich zu nehmen? Beruflich bin ich alle 3 Monate in Hamburg und kann sie nicht mitnehmen.'
    },
    {
        'title':'Fahrrad zu leihen gesucht',
        'image':'volker1/out/image93.jpeg',
        'category':'Sachen Gesuche',
        'text':'Am WE habe ich Besuch und brauche ein Fahrrad.'
    },
    {
        'title':'Kiezfonds Spenden',
        'image':'volker1/out/image6.jpg',
        'category':'Hilfe gesucht',
        'text':'Crowdfunding in der Nachbarschaft. Wir sammeln Geld für Initiativen, die die Nachbarschaft gestalten. Sei dabei!'
    },

]
groups = [
    {'name': 'Eltern Friedrichshain',
     'image': 'volker1/out/image1.jpg',
     'description': 'Hallo Eltern, vernetze dich in dieser Gruppe mit andern Eltern hier in der Nähe. Wir treffen uns regelmäßig und tauschen Sachen und Spielzeug und helfen uns beim Babysitting.',
     'members': ['Polly']
     },
    {'name': 'Language Tandem Gruppe',
     'image': 'volker1/out/image26.jpg',
     'description': 'Du möchtest eine Sprache lernen? Wir vermitteln einen Tandempartner.',
     'members': ['Polly']
     },

    {'name': 'Haustiergruppe',
     'image': 'volker1/out/image8.png',
     'description': 'Alles rund ums Thema Haustiere',
     },

    {'name': 'Singer Gruppe',
     'image': 'volker1/out/image9.jpg',
     'description': 'Komm und sing mit uns!',
     },
    {'name': 'Gruppe Wühlischstr. 12',
     'image': 'volker1/out/image10.jpg',
     'description': 'Kurzer Weg im Haus.',
     },
    {'name': 'Kita Sonnenschein',
     'image': 'volker1/out/image11.jpg',
     'description': 'Alle Eltern der Kita Sonnenschein.',
     },
    {'name': 'Klettergruppe',
     'image': 'volker1/out/image12.jpg',
     'description': 'Wir verabreden uns regelmäßig zum gemeinsamen klettern.',
     },
    {'name': 'Feierabendverteiler',
     'image': 'volker1/out/image13.jpg',
     'description': 'Nie wieder Langeweile am Feierabend.',
     },
    {'name': 'Lunchverteiler',
     'image': 'volker1/out/image14.jpg',
     'description': 'Gemeinsam Mittagessen in der Nachbarschaft.',
     },
    {'name': 'Hundegruppe',
     'image': 'volker1/out/image15.jpg',
     'description': 'Hundebesitzer und Hundefreunde, die keinen Hund haben, sind herzlich willkommen.',
     },
]

volker_users = [
    {
        'display_name':'Anne',
        'photo':'volker1/out/image126.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Joy',
        'photo':'volker1/out/image127.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Laura',
        'photo':'volker1/out/image129.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Barbara',
        'photo':'volker1/out/image130.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Beate',
        'photo':'volker1/out/image131.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Eva',
        'photo':'volker1/out/image133.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Kathleen',
        'photo':'volker1/out/image136.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Birte',
        'photo':'volker1/out/image137.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Annemarie',
        'photo':'volker1/out/image138.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Frauke',
        'photo':'volker1/out/image139.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Jade',
        'photo':'volker1/out/image146.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Birgit',
        'photo':'volker1/out/image152.png',
        'zip_code':'10243',
    },
    {
        'display_name':'Izzy',
        'photo':'volker1/out/image155.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Peter',
        'photo':'volker1/out/image175.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Michael',
        'photo':'volker1/out/image181.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Sean',
        'photo':'volker1/out/image183.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Jochen',
        'photo':'volker1/out/image191.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Vural',
        'photo':'volker1/out/image193.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Roberto',
        'photo':'volker1/out/image194.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Michal',
        'photo':'volker1/out/image197.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Justin',
        'photo':'volker1/out/image208.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Micki',
        'photo':'volker1/out/image210.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Henning',
        'photo':'volker1/out/image213.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Friedrich',
        'photo':'volker1/out/image215.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Rainer',
        'photo':'volker1/out/image232.jpg',
        'zip_code':'10243',
    },
    {
        'display_name':'Lars',
        'photo':'volker1/out/image234.jpg',
        'zip_code':'10243',
    },
]


for vu in volker_users:
    profile = {}
    vu['email'] = '%s@example.com' % vu['display_name']
    profile['about'] = ' ' # lorem_ipsum.paragraph()
    profile['photo'] = vu['photo']
    del vu['photo']
    vu['profile'] = profile

users = []
has_superuser = False
omg_password = 'p'
def mk_fh_point():
    center_lat = 52.5132939
    center_lon = 13.4533461

    length = random()
    length = (1-length**2) * 0.05

    direction = random()*2*pi
    off_lat = sin(direction)*length
    off_lon = cos(direction)*length

    lat = center_lat + off_lat
    lon = center_lon + off_lon

    return Point(lon, lat)


def image_name_to_django_file(fn):
    base_fn = os.path.basename(fn)
    fh = open(os.path.join(img_dir, fn), 'rb')
    dj_fh = File(fh)
    return base_fn, dj_fh

shuffle(volker_users)
volker_users = volker_users[:14]
for single_user_data in user_data + volker_users:
    photo = photo_name = None
    profile = None
    if 'profile' in single_user_data:
        profile = single_user_data['profile']
        del single_user_data['profile']
    if not has_superuser:
        has_superuser = True
        user = CBUser.objects.create_superuser(password=omg_password, email=single_user_data['email'])
        user.display_name = single_user_data['display_name']
    else:
        user = CBUser.objects.create(**single_user_data )
        user.set_password(omg_password)
    if profile:
        photo_name = profile['photo']
        photo_name, photo = image_name_to_django_file(photo_name)
        user.profile.photo.save(photo_name, photo)
        user.profile.about = profile['about']
        user.profile.save()

    user.position = mk_fh_point()
    user.save()
    users.append(user)

    if False:
        for i in range(randint(1,4)):
            listing = Listing.objects.create(
                title=lorem_ipsum.sentence()[:120],
                text=lorem_ipsum.paragraph(),
                creator=user,
                position=mk_fh_point() if random() > 0.5 else None
            )
            fn = choice(imgs)
            with open(os.path.join(img_dir, fn), 'rb') as fh:
                dj_fh = File(fh)
                listing.image.save(fn, dj_fh)
            listing.categories = set([choice(categories) for i in range(randint(1,4))])
            listing.save()

users = list(CBUser.objects.all())

bob = CBUser.objects.get(display_name='Bob')

for group_data in groups:
    fn,fh = image_name_to_django_file(group_data['image'])
    del group_data['image']
    group_data['position'] = mk_fh_point()
    members = []
    if 'members' in group_data:
        members = [CBUser.objects.get(display_name=m) for m in group_data['members']]
        del group_data['members']
    group = Group.objects.create(**group_data)
    group.members = list(users)[:15] + [bob]
    group.image.save(fn,fh)
    group.save()
for listing_data in listings:
    fn, fh = image_name_to_django_file(listing_data['image'])
    del listing_data['image']
    listing_data['position'] = mk_fh_point()
    listing_data['category'] = Category.objects.get_or_create(value=listing_data['category'])[0]
    listing_data['creator'] = choice(users)
    listing = Listing.objects.create(**listing_data)
    listing.image.save(fn, fh)

for listing in Listing.objects.all():
    for i in range(randint(1,6)):
        ListingComment.objects.create(
            listing=listing,
            author=choice(users),
            text='\n\n'.join(lorem_ipsum.paragraphs(randint(1,3)))
        )


