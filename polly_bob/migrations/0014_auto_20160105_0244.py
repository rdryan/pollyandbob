# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-05 02:44
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polly_bob', '0013_auto_20151217_1349'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='group',
            name='position',
            field=django.contrib.gis.db.models.fields.PointField(null=True, srid=4326),
        ),
    ]
