# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-13 15:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polly_bob', '0003_auto_20151213_1454'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tag',
            name='listing',
        ),
        migrations.AlterField(
            model_name='listing',
            name='categories',
            field=models.ManyToManyField(related_name='listings', to='community_base.Category'),
        ),
        migrations.DeleteModel(
            name='Tag',
        ),
    ]
