# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Achievement',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('description', models.CharField(max_length=40)),
                ('points', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='AchievementAward',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('achievement', models.ForeignKey(to='polly_bob.Achievement')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('text', models.TextField()),
                ('author', models.ForeignKey(related_name='comments', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
        migrations.CreateModel(
            name='Listing',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('title', models.CharField(max_length=120)),
                ('image', models.ImageField(upload_to='images')),
                ('text', models.TextField()),
                ('creator', models.ForeignKey(related_name='listings', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PollyData',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('unread', models.ManyToManyField(to='polly_bob.Listing')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, related_name='polly_data')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('value', models.CharField(max_length=40)),
                ('listing', models.ManyToManyField(related_name='tags', to='polly_bob.Listing')),
            ],
        ),
        migrations.AddField(
            model_name='comment',
            name='listing',
            field=models.ForeignKey(related_name='comments', to='polly_bob.Listing'),
        ),
        migrations.AddField(
            model_name='achievement',
            name='members',
            field=models.ManyToManyField(through='polly_bob.AchievementAward', to=settings.AUTH_USER_MODEL),
        ),
    ]
