# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import template
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter
def distance_m(value_dict):
    value = value_dict['distance']
    value = 100*int(value/100)
    return mark_safe('%i&nbsp;m' % value)

