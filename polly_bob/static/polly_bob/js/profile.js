/**
 * Created by goodfellow on 11/30/15.
 */
$(function(){
  $("form[method='post']").submit(function(e){
    e.preventDefault();
    $.post(
        $(this).attr('action'),
        $(this).serialize(),
        function(data){
          console.log(data)
        },
        "json"
    )
  });
});
