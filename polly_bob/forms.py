from __future__ import unicode_literals

from django import forms

from .models import Comment, Listing, Profile


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ()


class EmailNotificationsForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['notify_on_message', 'notify_on_request_or_review']
