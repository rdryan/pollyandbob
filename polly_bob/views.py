# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from random import shuffle
from django.contrib import auth
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin

from django.db.models import Max
from django.db.models.functions import Coalesce
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.shortcuts import render
from django.views.generic import UpdateView, TemplateView
from community_base.forms import ChangeEmailForm, ChangePasswordForm
from community_base.views import AjaxFormViewMixin
from community_base.models import Category
from .models import Listing, Profile, Group
from .forms import EmailNotificationsForm

def add_listing(request):
    request.is_ajax()

def add_comment(request):
    request.is_ajax()

def listings(request):
    filter_distance = 5000
    around_position = Point(13.4533461, 52.5132939)
    distance_tup = (around_position, filter_distance)

    listings = Listing.objects\
        .filter(position__distance_lte=distance_tup) \
        .annotate(latest_comment_date=Max('comments__created_at')) \
        .annotate(timestamp=Coalesce('latest_comment_date','created_at')) \
        .annotate(distance=Distance('position', around_position)) \
        .order_by('-timestamp')
    profiles = Profile.objects\
        .filter(user__position__distance_lte=distance_tup) \
        .annotate(distance=Distance('user__position', around_position)) \
        .order_by('user__created_at')
    groups = Group.objects\
        .filter(position__distance_lte=distance_tup) \
        .annotate(latest_comment_date=Max('threads__comments__created_at')) \
        .annotate(timestamp=Coalesce('latest_comment_date','created_at')) \
        .annotate(distance=Distance('position', around_position)) \
        .order_by('-timestamp')
    all = list(listings) + list(profiles) + list(groups)
    all.sort(key=lambda i: i.distance)
    all = [l.to_list_display() for l in all]
    return render(request, 'polly_bob/listings.html', {
        'items':all,
        'categories':Category.objects.all()
    })

def listing(request, listing_id):
    return render(request, 'polly_bob/listing.html', {'listing':Listing.objects.get(pk=listing_id)})

def group(request, group_id):
    group = Group.objects.get(pk=group_id)
    members = list(group.members.all())
    shuffle(members)
    return render(request, 'polly_bob/group.html', {'group':group, 'members':members})


def profile(request, user_id):
    pass


class AccountSettingsView(LoginRequiredMixin, TemplateView):
    template_name = 'polly_bob/settings.html'

    def get_context_data(self, **kwargs):
        kwargs['ChangePasswordForm'] = ChangePasswordForm()
        kwargs['EmailNotificationsForm'] = EmailNotificationsForm()
        kwargs['ChangeEmailForm'] = ChangeEmailForm()
        return super(AccountSettingsView, self).get_context_data(**kwargs)


class BaseUserUpdateView(AjaxFormViewMixin, LoginRequiredMixin, UpdateView):
    model = get_user_model()

    def get_object(self, queryset=None):
        return self.request.user


class EmailUpdateView(BaseUserUpdateView):
    form_class = ChangeEmailForm
    http_method_names = ['post']  # we save form from settings view
    #TODO: send email confirmation


class PasswordChangeView(BaseUserUpdateView):
    form_class = ChangePasswordForm
    http_method_names = ['post']  # we save form from settings view

    def form_valid(self, form):
        result = super().form_valid(form)
        user = self.object
        user = auth.authenticate(email=user.email,
                                 password=form.cleaned_data['new_password'])
        auth.login(self.request, user)
        return result


class NotificationUpdateView(BaseUserUpdateView):
    model = Profile
    form_class = EmailNotificationsForm
    http_method_names = ['post']  # we save form from settings view

    def get_object(self, queryset=None):
        return self.request.user.profile
