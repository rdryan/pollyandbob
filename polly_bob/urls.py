from django.conf.urls import patterns, include, url

from community_base.lib import make_simple_urlfunc
from .views import (
    AccountSettingsView, NotificationUpdateView, PasswordChangeView,
    EmailUpdateView)

pb_simple = make_simple_urlfunc('polly_bob')

urlpatterns = [
    url(r'^settings/$',
        AccountSettingsView.as_view(), name='profile_settings'),
    url(r'^settings/change-password$',
        PasswordChangeView.as_view(), name='profile_change_password'),
    url(r'^settings/change-email$',
        EmailUpdateView.as_view(), name='profile_change_email'),
    url(r'^settings/notifications/$',
        NotificationUpdateView.as_view(), name='profile_notifications'),
    pb_simple('listings')
]
