"""polly_bob_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import urls as dj_auth_urls

from community_base import api_urls
from community_base import urls as cb_urls
from polly_bob import urls as polly_bob_urls
from polly_bob.views import profile, listing, group

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/1.0/', include(api_urls.urlpatterns)),
    url(r'^', include(polly_bob_urls.urlpatterns)),
    url(r'^', include(dj_auth_urls.urlpatterns)),
    url(r'^pb/profile/(?P<user_id>[0-9]+)/$', profile, name='pb_profile'),
    url(r'^pb/listing/(?P<listing_id>[0-9]+)/$', listing, name='pb_listing'),
    url(r'^pb/group/(?P<group_id>[0-9]+)/$', group, name='pb_group'),
    url(r'^cb/', include(cb_urls.urlpatterns)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
