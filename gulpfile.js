var gulp = require('gulp');
var sass = require('gulp-sass');
var argv = require('yargs').argv;

var BUILD_DIR = 'build';
var BUILD_STATIC_OUT_DIR = BUILD_DIR + '/static-out';
var OUTPUT_DIR = 'static-out';
var IS_DEV = argv.dev !== undefined;

gulp.task('default', ['bootstrap-sass', 'css', 'js']);

gulp.task('bootstrap-sass', function() { 
    gulp.src(BUILD_STATIC_OUT_DIR + '/polly_bob/sass/bootstrap.sass')
         .pipe(
            sass() 
                .on('error', sass.logError)
        )
        .pipe(gulp.dest(BUILD_DIR + '/css')); 
});

gulp.task('css', function () {
    var postcss = require('gulp-postcss');
    var sourcemaps = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer-core');
    var atImport = require("postcss-import");
    var cssnano = require('cssnano');
    var concat = require('gulp-concat');
    var gulpif = require('gulp-if');

    return gulp.src([
        BUILD_DIR + '/css/*.css',
        BUILD_STATIC_OUT_DIR + '/polly_bob/css/*.css',
    ])
        .pipe(gulpif(!IS_DEV, sourcemaps.init()))
        .pipe(gulpif(!IS_DEV, postcss([
            atImport(),
            autoprefixer({browsers: ['last 2 versions']}),
            cssnano({zindex:false})
        ])))
        .pipe(gulpif(IS_DEV, postcss([atImport()])))
        .pipe(gulpif(!IS_DEV, sourcemaps.write('.')))
        .pipe(gulp.dest(OUTPUT_DIR + '/css'));
});

gulp.task('js', function () {
    var uglify = require('gulp-uglify');
    var concat = require('gulp-concat');
    var sourcemaps = require('gulp-sourcemaps');
    var gulpif = require('gulp-if');

    return gulp.src([
        'community_base/static/js/jquery-2.1.4.min.js',
        'community_base/static/js/jquery.cookie.js',
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        'community_base/static/js/community.js',
        BUILD_STATIC_OUT_DIR + '/lib/**/*.js'
    ])
        .pipe(gulpif(!IS_DEV, sourcemaps.init()))
        .pipe(concat('app.js'))
        .pipe(gulpif(!IS_DEV, uglify({mangle: false})))
        .pipe(gulpif(!IS_DEV, sourcemaps.write('.')))
        .pipe(gulp.dest(OUTPUT_DIR + '/js'));
});

gulp.task('watch', function () {
    gulp.watch(BUILD_STATIC_OUT_DIR + '/polly_bob/sass/*', ['bootstrap-sass']);
    gulp.watch([BUILD_DIR + '/css/*.css', BUILD_STATIC_OUT_DIR + '/css/*.css'], ['css']);
    gulp.watch(BUILD_STATIC_OUT_DIR + '/js/*.js', ['js']);
});
