import datetime, os

from fabric.api import settings, local, run as fab_run, env, cd, abort
from fabric.contrib.console import confirm
from fabric.contrib.project import rsync_project

def run():
    local('python manage.py runserver 0.0.0.0:8000')


def celery():
    local('celery -A proj worker --loglevel info')


def watch():
    local('gulp watch --dev')


def prepare_assets():
    try:
        os.mkdir('build')
    except OSError:
        pass
    local('python manage.py collectstatic --noinput')


def deploy():
    print('TODO')


def shell():
    local('python manage.py shell_plus')


def ws_srv():
    local('python -m community_base.ws-srv')
