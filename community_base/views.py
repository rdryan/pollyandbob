# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from braces.views import JSONResponseMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.decorators import login_required
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape
from django.utils.translation import ugettext_lazy as _
from django.views.generic import UpdateView, DetailView
from push_notifications.models import APNSDevice, GCMDevice
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.authentication import SessionAuthentication

from .models import Token, CBUser
from . import lib as clib


class CSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        pass


@api_view(['GET'])
@login_required
def token_info(request):
    token_id = request.META.get('HTTP_X_AUTH_TOKEN')
    token = Token.objects.get(pk=token_id)
    return clib.api_response_ok({'time_left':token.lifetime_left_secs})


@api_view(['POST'])
@authentication_classes((CSessionAuthentication,))
def open(request):
    if request.user and request.user.is_authenticated():
        user = request.user
    else:
        user = authenticate(email=request.POST.get('email'), password=request.POST.get('password'))
    if user is None:
        return clib.api_response_err(**clib.API_ERR_USER_UNKNOWN)
    if not user.is_active:
        return clib.api_response_err(**clib.API_ERR_ACCOUNT_LOCKED)
    new_token = Token.for_user(user)
    push_token = request.POST.get('apns_push_token', None)
    if push_token is not None:
        APNSDevice.objects.update_or_create(user=user, registration_id=push_token)

    push_token = request.POST.get('gcm_push_token', None)
    if push_token is not None:
        GCMDevice.objects.create(user=user, registration_id=push_token)
    result = {'token':new_token.id.hex, 'time_left':new_token.lifetime_left_secs}
    return clib.api_response_ok(result)


# TODO auth via shared secret?
@api_view(['GET'])
def token_valid(request, token):
    try:
        Token.objects.get(pk=token)
        return clib.api_response_ok(True)
    except Token.NotFound:
        return clib.api_response_ok(False)


@login_required
def block(request, partner_id):
    blocked = CBUser.objects.get(pk=partner_id)
    request.user.blocked.add(blocked)
    clib.django_message(
        request,
        mark_safe(_('<strong>%s</strong> kann Dich nun nicht mehr kontaktieren. Im Einstellungsmenü kannst Du Deine Sperren bearbeiten.') % conditional_escape(blocked.display_name)))
    return HttpResponseRedirect('/')


@login_required
def edit_blocked(request, extra_context=None):
    if request.method == 'POST':
        unblocked = CBUser.objects.get(pk=request.POST.get('unblock'))
        request.user.blocked.remove(unblocked)
        clib.django_message(
            request,
            mark_safe(_('Ok, <strong>%s</strong> kann Dich wieder kontaktieren.') % conditional_escape(unblocked.display_name)))
        return HttpResponseRedirect('/')
    else:
        if extra_context is None:
            extra_context = {}
        context = {'blocked': request.user.blocked.all()}
        context.update(extra_context)
        return render(request, 'edit_blocked.html', context)


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    fields = ['first_name', 'last_name', 'display_name']

    def get_object(self, queryset=None):
        return self.request.user


class ProfileDetailView(DetailView):
    model = get_user_model()
    slug_field = 'display_name'


class AjaxFormViewMixin(JSONResponseMixin):
    def form_valid(self, form):
        self.object = form.save()
        return self.render_json_response({
            'message': 'success!',
            'result': True
        })

    def form_invalid(self, form):
        return self.render_json_response({
            'message': 'fail',
            'errors': form.errors,
            'result': False
        })

    def get_context_data(self, **kwargs):
        return {'form': self.form_class()}
