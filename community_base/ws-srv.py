# run with python -m community_base.ws-srv
import asyncio
import aiohttp
from aiohttp import web
import itertools
from . import api_messages
from collections import defaultdict

client_registry = defaultdict(lambda: [])

def _do_send_msg(data, close_after_send=False):
    if 'to' in data:
        clients = itertools.chain.from_iterable(client_registry[k] for k in data['to'].split(','))
    else:
        # brrrrrroadcast
        clients = itertools.chain.from_iterable(client_registry.values())
    msg = data.get('msg', None)
    if msg is None:
        return web.Response(body=b'ey')
    sent = False
    for client in clients:
        sent = True
        client.send_str(msg)
        if close_after_send:
            client.close()
    return sent

@asyncio.coroutine
def send_msg(request):
    data = yield from request.post()
    if _do_send_msg(data):
        return web.Response(body=api_messages.api_response_ok_body(None))
    else:
        return web.Response(body=api_messages.api_response_err_body(**api_messages.API_ERR_NOT_FOUND), status=404)

@asyncio.coroutine
def expire(request):
    data = yield from request.post()
    ids = data['ids'].split(',')
    print('yo expire %s' % ids)
    _do_send_msg({'to':ids, 'msg':'["expire",null]'}, close_after_send=True)
    return web.Response(body=api_messages.api_response_ok_body(None))

@asyncio.coroutine
def list_clients(request):
    return web.Response(body='\n'.join(client_registry.keys()).encode('utf-8'))

@asyncio.coroutine
def handle_websocket(request):
    ws = web.WebSocketResponse()
    ws.start(request)

    client_id = None
    while True:
        msg = yield from ws.receive()
        if msg.tp == aiohttp.MsgType.close:
            print('close')
            if client_id is not None:
                clients = client_registry[client_id]
                clients.remove(ws)
                if not clients:
                    print('%s: last client disconnected' % client_id)
                    del client_registry[client_id]
                    print(client_registry)
                else:
                    print('%s: %i left' % (client_id, len(clients)))
            break

        elif msg.tp == aiohttp.MsgType.text:
            # the only thing a client ever sends us is his token/id
            client_id = msg.data
            clients = client_registry[client_id]
            clients.append(ws)
            print('client %s registered, now:%i' % (client_id, len(clients)))
    return ws


app = web.Application()
app.router.add_route('POST', '/send', send_msg)
app.router.add_route('POST', '/expire', expire)
app.router.add_route('GET', '/clients', list_clients)
app.router.add_route('GET', '/', handle_websocket)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    handler = app.make_handler()
    host = '0.0.0.0'
    port = 9001
    print('listening on %s:%s' % (host,port))
    srv = loop.run_until_complete(loop.create_server(handler, host, port))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(handler.finish_connections(1.0))
        srv.close()
        loop.run_until_complete(srv.wait_closed())
        loop.run_until_complete(app.finish())
    loop.close()
