# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import phonenumber_field.modelfields
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CBUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name='Email')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='Vorname', null=True)),
                ('last_name', models.CharField(blank=True, max_length=30, verbose_name='Nachname', null=True)),
                ('display_name', models.CharField(blank=True, unique=True, max_length=128, verbose_name='Angezeigter Name', null=True)),
                ('street', models.CharField(blank=True, max_length=128, verbose_name='Straße', null=True)),
                ('zip_code', models.CharField(db_index=True, null=True, max_length=5, verbose_name='Postleitzahl', blank=True)),
                ('city', models.CharField(blank=True, max_length=128, verbose_name='Stadt', null=True)),
                ('country', models.CharField(blank=True, max_length=128, verbose_name='Land', null=True)),
                ('push_token', models.CharField(blank=True, max_length=4096, null=True)),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, verbose_name='Telefonnummer', null=True)),
                ('is_verified_phone', models.BooleanField(default=False)),
                ('is_verified_email', models.BooleanField(default=False)),
                ('blocked', models.ManyToManyField(related_name='blocked_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BaseImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='images')),
                ('added', models.DateTimeField(auto_now_add=True)),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ['added'],
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=255)),
                ('order', models.PositiveIntegerField()),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='Subcategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=255)),
                ('order', models.PositiveIntegerField()),
                ('category', models.ForeignKey(related_name='subcategories', to='community_base.Category')),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='Token',
            fields=[
                ('id', models.UUIDField(primary_key=True, serialize=False, editable=False, default=uuid.uuid4)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Verify',
            fields=[
                ('mode', models.CharField(choices=[('p', 'Phone'), ('e', 'Email')], max_length=1)),
                ('id', models.UUIDField(primary_key=True, serialize=False, editable=False, default=uuid.uuid4)),
                ('user', models.OneToOneField(related_name='verify', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
