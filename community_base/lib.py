# -*- coding: utf-8 -*-

from django.http import HttpResponseServerError
from django.utils import timezone
from django.conf.urls import patterns, include, url
from django.contrib import messages as django_messages
from django.conf import settings


from rest_framework.response import Response

from .models import Token
from .api_messages import *

from dateutil.relativedelta import relativedelta
import requests


def make_simple_urlfunc(app_name):
    def f(name):
        names = (app_name, name)
        return url(r'^%s/$' % name, '%s.views.%s' % names, name = "%s.%s" % names)
    return f

def api_response_ok(result):
    return Response(ok_dict(result))

def api_response_err(
        message=DEFAULT_ERROR_MESSAGE,
        code=DEFAULT_ERROR_CODE):
    result = Response(data=err_dict(message, code))
    return result

def expire_tokens():
    if Token.is_immortal(): return
    tokens = Token.objects.filter(created_at__lt=timezone.now() - relativedelta(seconds=Token.expire_secs()))
    requests.post(settings.CB_WS_URL+'expire', {'ids':tokens.ids()})
    tokens.delete()

BOOTSTRAP_LEVEL_SUCCESS = 'alert-success'
BOOTSTRAP_LEVEL_INFO = 'alert-info'
BOOTSTRAP_LEVEL_WARNING = 'alert-warning'
BOOTSTRAP_LEVEL_DANGER = 'alert-danger'

def django_message(request, message, django_level=None, bootstrap_level=BOOTSTRAP_LEVEL_INFO):
    if not django_level:
        django_level = {
            BOOTSTRAP_LEVEL_DANGER: django_messages.ERROR
        }.get(bootstrap_level, django_messages.INFO)
    django_messages.add_message(request, django_level, message, extra_tags=bootstrap_level)
