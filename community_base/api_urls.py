from django.conf.urls import patterns, include, url
from community_base.views import open, token_valid

urlpatterns = [
    url(r'^open/$', open, name='api.open'),
    url(r'^token_valid/$', token_valid, name='token_valid'),
    ]
