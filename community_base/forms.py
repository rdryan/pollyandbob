from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
# from jade.forms import mofo


class RegisterForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ('email', 'password1', 'password2', 'display_name', 'phone')
    password1 = forms.CharField(
        label=_('Passwort'),
        widget=forms.PasswordInput(render_value=True))
    password2 = forms.CharField(
        label=_('Passwortwiederholung'),
        widget=forms.PasswordInput(render_value=True),
        help_text=_('Gib Dein Passwort zur Überprüfung ein zweites Mal ein.'))
    phone = forms.CharField(
        required=False,
        label = _('Handynummer'),
        help_text=_('Optional - für Benachrichtigungen von Jade, bequeme Rückrufe etc.')
    )
    display_name = forms.CharField(
        label = _('Angezeigter Name'),
        help_text=_('Unter diesem Namen kontaktierst Du andere.')
    )

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user


class ChangePasswordForm(forms.ModelForm):
    old_password = forms.CharField(
        widget=forms.PasswordInput, label=_('Alt passwort'))
    new_password = forms.CharField(
        30, 6, widget=forms.PasswordInput, label=_('Neu passwort'))
    new_password2 = forms.CharField(
        30, 6, widget=forms.PasswordInput, label=_('Neu passwortwiederholung'))

    class Meta:
        model = get_user_model()
        fields = []

    def clean_old_password(self):
        old_password = self.cleaned_data.get('old_password')
        if not self.instance.check_password(old_password):
            raise forms.ValidationError("Password incorrect")
        return old_password

    def clean_new_password2(self):
        # Check that the two password entries match
        new_password = self.cleaned_data.get('new_password')
        new_password2 = self.cleaned_data.get('new_password2')
        if new_password and new_password2 and new_password != new_password2:
            raise forms.ValidationError("Passwords don't match")
        return new_password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['new_password'])
        if commit:
            user.save()
        return user


class ChangeEmailForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['email']

    def save(self, commit=True):
        super(ChangeEmailForm, self).save(commit)
        #TODO: send activation email


class DeleteAccountForm(forms.Form):
    delete = forms.RadioSelect(choices=['no', 'yes'])
