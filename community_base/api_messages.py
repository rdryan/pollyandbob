import json

we_solved_this_problem = False
if we_solved_this_problem:
    from django.utils.translation import ugettext_lazy as _
    from django.utils.functional import Promise
    from django.utils.encoding import force_text
    from django.core.serializers.json import DjangoJSONEncoder

# helper to encode lazily translated strings to json
if we_solved_this_problem:
    class LazyEncoder(DjangoJSONEncoder):
        def default(self, obj):
            if isinstance(obj, Promise):
                return force_text(obj)
            return super(LazyEncoder, self).default(obj)
    Encoder = LazyEncoder
else:
    Encoder = json.JSONEncoder

# ok, nice try there with the LazyEncoder, but it turns out we need to initialize django to use it,
# which is kinda lame if we want to be  moreindependent of it, sooo....

_ = lambda sadface: sadface

API_ERR_USER_UNKNOWN = {'message':_('user unknown'), 'code':0}
API_ERR_ACCOUNT_LOCKED = {'message':_('account locked'), 'code':1}
API_ERR_UNAUTHORIZED = {'message':_('unauthorized'), 'code':401}
API_ERR_NOT_FOUND = {'message':_('not found'), 'code':404}

DEFAULT_ERROR_MESSAGE = _('something went wrong')
DEFAULT_ERROR_CODE = -1

def ok_dict(result):
    return {
        'status': 'ok',
        'data': result}

# X_X is NOT localized on purpose
def err_dict(message=('X_X'), code=-1):
    return {
        'status': 'error',
        'message': message,
        'code': code}

def api_response_ok_body(result):
    return json.dumps(ok_dict(result), cls=Encoder).encode('utf-8')

def api_response_err_body(message=DEFAULT_ERROR_MESSAGE, code=DEFAULT_ERROR_CODE):
    return json.dumps(err_dict(message, code), cls=Encoder).encode('utf-8')
