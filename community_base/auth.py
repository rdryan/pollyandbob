# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.contrib.auth.backends import ModelBackend
from rest_framework import authentication
from rest_framework import exceptions

from .models import Token, Verify, CBUser


class TokenBackend(ModelBackend):
    def authenticate(self, token_id=None):
        try:
            token = Token.objects.get(pk=token_id)
            if token.is_valid:
                return token.user
            else:
                token.delete()
        except Token.DoesNotExist:
            pass


class VerifyBackend(ModelBackend):
    def authenticate(self, verify_id=None):
        try:
            verify = Verify.objects.get(pk=verify_id)
            return verify.user
        except Verify.DoesNotExist:
            pass


class HttpResponseUnauthorized(HttpResponse):
    status_code = 401


class TokenAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        token_id = request.META.get('HTTP_X_AUTH_TOKEN')
        if not token_id:
            return None
        user = None
        try:
            token = Token.objects.get(pk=token_id)
            if not token.is_valid:
                token.delete()
                raise exceptions.AuthenticationFailed('Token expired')
            user = token.user
        except (Token.DoesNotExist, CBUser.DoesNotExist, ValueError):
            raise exceptions.AuthenticationFailed('Token or user invalid')

        return user, None
