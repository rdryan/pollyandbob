# -*- coding: utf-8 -*-
from django.conf.urls import url

from community_base.views import ProfileUpdateView, ProfileDetailView


urlpatterns = [
    url(r'^update-profile/',
        ProfileUpdateView.as_view(), name='profile_update'),
    url(r'^id(?P<pk>\d+)', ProfileDetailView.as_view(), name='profile_detail'),
    url(r'^(?P<slug>\w+)', ProfileDetailView.as_view(), name='profile_detail')
]
