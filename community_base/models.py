# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import uuid

import structlog
from django.contrib.gis.db import models
from django.db import connection
from django.core.urlresolvers import reverse
try:
    # django <=1.8
    from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
except ImportError:
    # django >=1.9
    from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.conf import settings
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField

logger = structlog.get_logger()


def prefetch_id(instance):
    """ Fetch the next value in a django id autofield postgresql sequence """
    cursor = connection.cursor()
    cursor.execute(
        "SELECT nextval('{0}_{1}_id_seq'::regclass)".format(
            instance._meta.app_label.lower(),
            instance._meta.object_name.lower(),
        )
    )
    row = cursor.fetchone()
    cursor.close()
    return int(row[0])


class CBUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class CBUser(AbstractBaseUser):
    # django stuffs

    USERNAME_FIELD = 'email'
    objects = CBUserManager()
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    email = models.EmailField(_('Email'), max_length=255, unique=True)

    first_name = models.CharField(_('Vorname'), max_length=30, blank=True, null=True)
    last_name = models.CharField(_('Nachname'), max_length=30, blank=True, null=True)

    display_name = models.CharField(
        _('Angezeigter Name'), max_length=128, null=True, blank=True, db_index=True,
        unique=True)  # don't forget to validate it in signup form
    street = models.CharField(_('Straße'), max_length=128, null=True, blank=True)
    zip_code = models.CharField(_('Postleitzahl'), max_length=5, db_index=True, null=True, blank=True)  #TODO validate
    city = models.CharField(_('Stadt'), max_length=128, null=True, blank=True)
    country = models.CharField(_('Land'), max_length=128, null=True, blank=True)
    position = models.PointField(null=True)  # derived from address or updated manually
    push_token = models.CharField(max_length=4096, null=True, blank=True)  #lol, google
    phone = PhoneNumberField(_('Telefonnummer'), blank = True, null = True)
    blocked = models.ManyToManyField('self', symmetrical=False, related_name='blocked_by', blank=True)

    is_verified_phone = models.BooleanField(default=False)
    is_verified_email = models.BooleanField(default=False)

    def get_absolute_url(self):
        if self.display_name:
            return reverse('profile_detail', kwargs={'slug': self.display_name})
        return reverse('profile_detail', args=[self.pk])

    @property
    def is_verified(self):
        return self.is_verified_phone or self.is_verified_email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        """Does the user have a specific permission?"""
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        """Does the user have permissions to view the app `app_label`?"""
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        """Is the user a member of staff?"""
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def __str__(self):
        if self.display_name:
            return "%s (%s)" % (self.display_name, self.email)
        else:
            return "%s" % self.email

    def save(self, *args, **kwargs):
        if not self.email:
            self.id = prefetch_id(self)
            self.email = '%s@anonymous.example.com' % self.id
            self.display_name = _("Anonym") + ' %s' % self.id

        super().save(*args, **kwargs)


class BaseImage(models.Model):
    image = models.ImageField(upload_to='images')
    added = models.DateTimeField(auto_now_add=True)
    description = models.TextField()

    class Meta:
        ordering = ['added']


class Category(models.Model):
    value = models.CharField(max_length=255, db_index=True)
    order = models.PositiveIntegerField()

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.value


class Subcategory(models.Model):
    value = models.CharField(max_length=255, db_index=True)
    order = models.PositiveIntegerField()
    category = models.ForeignKey(Category, related_name='subcategories')

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.value


class TokenQuerySet(models.QuerySet):
    def ids(self):
        return ','.join(o[0].hex for o in self.values_list('id'))


class Token(models.Model):
    objects = TokenQuerySet.as_manager()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(CBUser)

    @classmethod
    def expire_secs(cls):
        return getattr(settings, 'WS_AUTHTOKEN_EXPIRE_SECS', 3600)

    @classmethod
    def is_immortal(cls):
        return Token.expire_secs() <= 0

    @classmethod
    def for_user(cls, user):
        result = None
        try:
            existing = Token.objects.filter(user=user).order_by('-created_at')[0:1].get()
            if Token.is_immortal() or existing.lifetime_left_secs > Token.expire_secs() / 2:
                result = existing
        except Token.DoesNotExist:
            pass
        if not result:
            result = Token.objects.create(user=user)
        return result

    @property
    def lifetime_left_secs(self):
        if Token.is_immortal():
            return None
        return Token.expire_secs() - (timezone.now() - self.created_at).total_seconds()

    @property
    def is_valid(self):
        return Token.is_immortal() or self.lifetime_left_secs > 0


class Verify(models.Model):
    VERIFY_PHONE = 'p'
    VERIFY_EMAIL = 'e'
    VERIFY_CHOICES = (
        (VERIFY_PHONE, 'Phone'),
        (VERIFY_EMAIL, 'Email'),
    )
    mode = models.CharField(max_length=1, choices=VERIFY_CHOICES)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(CBUser, related_name='verify')

    @classmethod
    def create(cls, user):
        result = cls(user=user)
        result.mode = Verify.VERIFY_PHONE if user.phone else Verify.VERIFY_EMAIL
        result.save()
        return result

    def __str__(self):
        return "Verify code %s for %s" % (self.id, self.user)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('value', 'subs', 'order')

    def subs(self, obj):
        soup = []
        for sub in obj.subcategories.all():
            soup.append('<a href="%s">%s (%s)</a><br>' % (reverse(
                'admin:jade_subcategory_change', args=(sub.id,)
            ), sub.value, sub.order))
        return ''.join(soup)
    subs.allow_tags = True
    subs.short_description = 'Subcategories'

for cls, adminCls in [[Category, CategoryAdmin]]:
    admin.site.register(cls, adminCls)

for admin_cls in [CBUser, Token, Subcategory, Verify]:
    admin.site.register(admin_cls)


from django_cleanup.signals import cleanup_pre_delete


def sorl_delete(**kwargs):
    from sorl.thumbnail import delete
    delete(kwargs['file'])


cleanup_pre_delete.connect(sorl_delete)
